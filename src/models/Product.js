const DataTypes = require('sequelize');
const sequelize = require('../config/sequelize');

const Product = sequelize.define('Product', {
    prodID: {
        type: DataTypes.INTEGER,
        allowNull: false,
        primaryKey: true,
        autoIncrement: true
    },
    pics: {
        type: DataTypes.TEXT,
        allowNull: true
    },
    name: {
        type: DataTypes.STRING,
        allowNull: false
    },
    price: {
        type: DataTypes.FLOAT,
        allowNull: false
    },
    description: {
        type: DataTypes.TEXT,
        allowNull: true
    },
    usage: {
        type: DataTypes.DATEONLY,
        allowNull: true
    }
});

Product.associate = function(models) {
    Product.belongsTo(models.User);
};

module.exports = Product;