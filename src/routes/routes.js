const { Router } = require('express');
const router = Router();

const UserController = require('../controllers/UserController');
const ProductController = require('../controllers/ProductController');

//ROTAS USUÁRIO
router.get('/user', UserController.index);
router.get('/user/:id', UserController.show);
router.post('/user', UserController.create);
router.put('/user/:id', UserController.update);
router.delete('/user/:id', UserController.destroy);

// ROTAS PRODUTO
router.get('/product', ProductController.index);
router.get('/product/:id', ProductController.show);
router.post('/product', ProductController.create);
router.put('/product/:id', ProductController.update);
router.delete('/product/:id', ProductController.destroy);

// PRODUTO -> USUÁRIO_Vendedor
router.put('/product/pts/:product_id/seller/:user_id', ProductController.productToSeller);
router.put('/product/rfs/:product_id', ProductController.removeFromSeller);

module.exports = router;