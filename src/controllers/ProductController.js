const {response} = require('express');
const Product = require('../models/Product');
const User = require('../models/User');

const create = async(req, res) => {
    try {
        const product = await Product.create(req.body);
        return res.status(201).json({message: "ok", product: product});
    }
    catch(err) {
        res.status(500).json({error: err});
    }
};

const index = async(req, res) => {
    try {
        const product = await Product.findAll();
        return res.status(200).json({product});
    }
    catch(err) {
        res.status(500).json({err});
    }
};

const show = async(req, res) => {
    const {id} = req.params;

    try {
        const product = await Product.findByPk(id);
        return res.status(200).json({product});
    }
    catch(err) {
        return res.status(500).json({err});
    }
};

const update = async(req, res) => {
    const {id} = req.params;

    try {
        const [updated] = await Product.update(req.body, {where:{prodID: id}});
        
        if(updated) {
            const product = await Product.findByPk(id);
            return res.status(200).send(product);
        }
        
        throw new Error();
    }
    catch(err) {
        return res.status(500).json({err});
    }
};

const destroy = async(req, res) => {
    const {id} = req.params;

    try {
        const deleted = await Product.destroy({where:{prodID: id}});
        if(deleted) {
            return res.status(200).json("Produto deletado com sucesso.");
        }
        throw new Error();
    }
    catch(err) {
        return res.status(500).json("Produto não encontrado.");
    }
};

const productToSeller = async(req, res) => {
    const {product_id, user_id} = req.params;

    try {
        const product = await Product.findByPk(product_id);
        const seller = await User.findByPk(user_id)

        await product.setUser(seller);
        return res.status(200).json({message: "Produto adicionado ao vendedor.", product: product, seller: seller});
    }
    catch(err) {
        return res.status(500).json({err});
    }
};

const removeFromSeller = async(req, res) => {
    const {product_id} = req.params;
    
    try {
        const product = await Product.findByPk(product_id);
        await product.setUser(null);
        
        return res.status(200).json({message: "Produto removido do vendedor.", product: product});
    }
    catch(err) {
        return res.status(500).json({err});
    }

};

module.exports = {
    create,
    index,
    show,
    update,
    destroy,
    productToSeller,
    removeFromSeller
};